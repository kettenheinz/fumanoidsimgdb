% baseDir = path to Image database  ../FUmanoids/
% posDir  = Folder with new positive images  /Train/pos
% annDir  = Folder for annotations /Train/annotations

function annotateImages(startNum, baseDir, posDir, annDir, name)
	name = name;
	ext = ".png";
	dir_base = baseDir;
	dir_pos = posDir;
	dir_ann = annDir;
	label = '"FURobot"';

	% remove trailing slash from the dir_base if need be
	if isequal(dir_base(end), '/') dir_base = dir_base(1:end-1); end

	%get the contents of the folder
	contents = dir([dir_base dir_pos]);
	% get number of image files, -2 because octave counts . and .. as well
	numFiles = numel(contents)-2;
	% data to be written into a textifle
	data = cell (numFiles,5);

	handle = figure;
	% loop through all images
  startNum
	for n = startNum:(numFiles)
    n
	%	[lead name ext] = fileparts(name);
		im = imread([dir_base dir_pos name sprintf('%04d', n) ext]);
    "read image"
		[imWidth, imHeight, imDepth] = size(im);
		hold off; imagesc(im); axis image; axis off; hold on;

    %determine number of objects
		title('Type the number of objects you want to annotate in this picture');
    num_objects = input('Type the number of objects you want to annotate in this picture: ');
    %-------------------------------------------------------


		data(n,1) = {[dir_pos name sprintf('%04d', n) ext]};     %filepath


		%write information about image into separate text file
		fid = fopen([dir_base dir_ann name sprintf('%04d', n) '.txt'], 'w');
		fprintf(fid, '# FUmanoid Annotation Version 0.1\n\n');

		fprintf(fid, 'Image filename : "%s" \n', data{n,1});
		fprintf(fid, 'Image size (X x Y x C) : %i x %i x %i \n', imWidth, imHeight, imDepth);
		fprintf(fid, 'Database : "The FUmanoids Berlin annotated robot database" \n');
		fprintf(fid, 'Objects with ground truth : %i {%s} \n\n', num_objects, label);

		fprintf(fid, '# Note that there might be other objects in the image\n');
		fprintf(fid, '# for which ground truth data has not been provided.\n\n');

		fprintf(fid, '# Top left pixel co-ordinates : (0, 0) \n\n');

    for m = 1:num_objects
      m
      % annotate center of object's head (not needed)
		  %title('Click on the center corner of the object');
		  %[centerX, centerY, button] = ginput(1);

      % annotate the bounding box
		  title('First click on the top left corner of the object and then on the bottom right one');
		  [x, y, button] = ginput(2);
		  plot(x,y,'g.');

		  data(n,2) = x(1,1);	%bounding box top left
		  data(n,3) = y(1,1);
		  data(n,4) = x(2,1); %bounding box bottom right
		  data(n,5) = y(2,1);

      %write data for each object into file
		  fprintf(fid, '# Details for object %i (%s) \n', m, label);
		  fprintf(fid, '# Center point -- not available in this databases -- refers to robot head center \n');
		  fprintf(fid, 'Original label for object %i %s : "upright robot" \n', m, label);
		  %fprintf(fid, 'Center point on object %i %s (X, Y) : (%i, %i) \n', m, label, centerX, centerY);
      fprintf(fid, 'Center point on object %i %s (X, Y) : (%i, %i) \n', m, label, 50, 50);
		  fprintf(fid, 'Bounding box for object %i "Robot" (Xmin, Ymin) - (Xmax, Ymax) : (%i, %i) - (%i, %i) \n\n', m, data{n,2:5});
    end
		fclose(fid);
	end

	close(handle);
end
