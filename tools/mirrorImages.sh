#! /bin/bash

#Create a mirrored version of each image in a new folder.
#@params: $1= path

#Das Script spiegelt alle Bilder nach der angegebenen Startzahl und schreibt sie in einen neuen Ordner. Danach werden die Werte in den dazugehörigen Annotationen ebenfalls #gespiegelt und in einem separaten Ordner in neue Dateien geschrieben. Anschließend werden die Dateien korrekt mit fortlaufenden Nummern benannt und jeweils dem /pos und /#annotations Ordnern hinzugefügt. Abschließend werden die temporären Ordner gelöscht.





path=$1
a=1

for i in $path*.png; do
  file="${i##*/}"
  newFile=$(printf "%s_flop.png" "$file")
  newPath=$(printf "%smirrored/" "$path")
  mkdir $newPath
  convert $path$file  -flop  $newPath$newFile
  let a=a+1
done

echo "Done mirroring."
