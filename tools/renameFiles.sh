#! /bin/bash

#Rename all .png files in a given folder. The new name contains a string and four digits: robot_0471.png
#@params: $1= path, $2= new name string, §3= starting number $4= file type

path=$1
name=$2
num=$3
ext=$4

for i in $path*.$ext; do
  new=$(printf "%s%04d.%s" "$name" "$num" "$ext") #04 pad to length of 4
  mv -- "$i" "$path$new"
  let num=num+1
done

echo "Done renaming."
