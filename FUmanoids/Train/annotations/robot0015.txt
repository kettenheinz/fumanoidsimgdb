# FUmanoid Annotation Version 0.1

Image filename : "/Train/pos/robot0015.png" 
Image size (X x Y x C) : 3443 x 3927 x 3 
Database : "The FUmanoids Berlin annotated robot database" 
Objects with ground truth : 3 {"FURobot"} 

# Note that there might be other objects in the image
# for which ground truth data has not been provided.

# Top left pixel co-ordinates : (0, 0) 

# Details for object 1 ("FURobot") 
# Center point -- not available in this databases -- refers to robot head center 
Original label for object 1 "FURobot" : "upright robot" 
Center point on object 1 "FURobot" (X, Y) : (50, 50) 
Bounding box for object 1 "Robot" (Xmin, Ymin) - (Xmax, Ymax) : (21, 624) - (618, 1735) 

# Details for object 2 ("FURobot") 
# Center point -- not available in this databases -- refers to robot head center 
Original label for object 2 "FURobot" : "upright robot" 
Center point on object 2 "FURobot" (X, Y) : (50, 50) 
Bounding box for object 2 "Robot" (Xmin, Ymin) - (Xmax, Ymax) : (846, 1088) - (2091, 2828) 

# Details for object 3 ("FURobot") 
# Center point -- not available in this databases -- refers to robot head center 
Original label for object 3 "FURobot" : "upright robot" 
Center point on object 3 "FURobot" (X, Y) : (50, 50) 
Bounding box for object 3 "Robot" (Xmin, Ymin) - (Xmax, Ymax) : (3316, 408) - (3907, 1259) 

