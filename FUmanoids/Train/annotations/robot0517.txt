# FUmanoid Annotation Version 0.1

Image filename : "/Train/pos/robot0517.png" 
Image size (X x Y x C) : 480 x 550 x 3 
Database : "The FUmanoids Berlin annotated robot database" 
Objects with ground truth : 4 {"FURobot"} 

# Note that there might be other objects in the image
# for which ground truth data has not been provided.

# Top left pixel co-ordinates : (0, 0) 

# Details for object 1 ("FURobot") 
# Center point -- not available in this databases -- refers to robot head center 
Original label for object 1 "FURobot" : "upright robot" 
Center point on object 1 "FURobot" (X, Y) : (50, 50) 
Bounding box for object 1 "Robot" (Xmin, Ymin) - (Xmax, Ymax) : (46, 45) - (155, 208) 

# Details for object 2 ("FURobot") 
# Center point -- not available in this databases -- refers to robot head center 
Original label for object 2 "FURobot" : "upright robot" 
Center point on object 2 "FURobot" (X, Y) : (50, 50) 
Bounding box for object 2 "Robot" (Xmin, Ymin) - (Xmax, Ymax) : (274, 30) - (368, 182) 

# Details for object 3 ("FURobot") 
# Center point -- not available in this databases -- refers to robot head center 
Original label for object 3 "FURobot" : "upright robot" 
Center point on object 3 "FURobot" (X, Y) : (50, 50) 
Bounding box for object 3 "Robot" (Xmin, Ymin) - (Xmax, Ymax) : (418, 62) - (525, 219) 

# Details for object 4 ("FURobot") 
# Center point -- not available in this databases -- refers to robot head center 
Original label for object 4 "FURobot" : "upright robot" 
Center point on object 4 "FURobot" (X, Y) : (50, 50) 
Bounding box for object 4 "Robot" (Xmin, Ymin) - (Xmax, Ymax) : (162, 209) - (322, 454) 

