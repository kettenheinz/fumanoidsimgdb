# FUmanoid Annotation Version 0.1

Image filename : "/Train/pos/robot0021.png" 
Image size (X x Y x C) : 2691 x 3325 x 3 
Database : "The FUmanoids Berlin annotated robot database" 
Objects with ground truth : 3 {"FURobot"} 

# Note that there might be other objects in the image
# for which ground truth data has not been provided.

# Top left pixel co-ordinates : (0, 0) 

# Details for object 1 ("FURobot") 
# Center point -- not available in this databases -- refers to robot head center 
Original label for object 1 "FURobot" : "upright robot" 
Center point on object 1 "FURobot" (X, Y) : (50, 50) 
Bounding box for object 1 "Robot" (Xmin, Ymin) - (Xmax, Ymax) : (2206, 680) - (3319, 2685) 

# Details for object 2 ("FURobot") 
# Center point -- not available in this databases -- refers to robot head center 
Original label for object 2 "FURobot" : "upright robot" 
Center point on object 2 "FURobot" (X, Y) : (50, 50) 
Bounding box for object 2 "Robot" (Xmin, Ymin) - (Xmax, Ymax) : (754, 244) - (1716, 1809) 

# Details for object 3 ("FURobot") 
# Center point -- not available in this databases -- refers to robot head center 
Original label for object 3 "FURobot" : "upright robot" 
Center point on object 3 "FURobot" (X, Y) : (50, 50) 
Bounding box for object 3 "Robot" (Xmin, Ymin) - (Xmax, Ymax) : (1, 56) - (792, 1599) 

