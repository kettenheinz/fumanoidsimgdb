# FUmanoid Annotation Version 0.1

Image filename : "/Train/pos/robot0133.png" 
Image size (X x Y x C) : 1309 x 803 x 3 
Database : "The FUmanoids Berlin annotated robot database" 
Objects with ground truth : 1 {"FURobot"} 

# Note that there might be other objects in the image
# for which ground truth data has not been provided.

# Top left pixel co-ordinates : (0, 0) 

# Details for object 1 ("FURobot") 
# Center point -- not available in this databases -- refers to robot head center 
Original label for object 1 "FURobot" : "upright robot" 
Center point on object 1 "FURobot" (X, Y) : (50, 50) 
Bounding box for object 1 "Robot" (Xmin, Ymin) - (Xmax, Ymax) : (24, 48) - (748, 1241) 

